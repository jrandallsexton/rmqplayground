﻿
using System.Collections.Generic;

using fDox.Common.Dtos;

namespace fDox.Common.Shared
{
    public class UpdateAircraftDueDateCommand
    {
        public Aircraft Aircraft { get; set; }
        public List<TaskItem> Tasks { get; set; }
    }
}