﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using fDox.Common.Dtos;
using fDox.Common.Messaging;
using fDox.Common.Shared;
using Newtonsoft.Json;
using RabbitMQ.Client;

namespace Publisher
{
    class Program
    {
        static void Main(string[] args)
        {
            var useRpc = true;

            if (useRpc)
            {
                for (var i = 0; i < 1; i++)
                {
                    for (var j = 1; j < 3; j++)
                    {
                        var aircraft = GetTestAircraft(j);

                        var command = new UpdateAircraftDueDateCommand()
                        {
                            Aircraft = aircraft,
                            Tasks = GenerateTestTasks()
                        };
                        Console.WriteLine("Item generated");
                        SendRmqMessageViaRpc(command);
                    }
                }

                Console.WriteLine("Items sent via RPC. Any key to exit");
                Console.ReadLine();
            }
        }

        public static void SendRmqMessageViaRpc(UpdateAircraftDueDateCommand command)
        {
            var config = new MessageServerConfig
            {
                Host = "portal1394-7.euphoric-rabbitmq-75.752837871.composedb.com",
                Port = 16456,
                Username = "fdox",
                Password = "aviation1234",
                VirtualHost = "euphoric-rabbitmq-75",
                ExchangeName = string.Empty,
                QueueName = "aircraft.que.fdox"
            };

            var rpcClient = new RpcClient(config);
            Console.WriteLine("Sending");

            var msg = JsonConvert.SerializeObject(command);
            var response = rpcClient.CallAsync(msg, string.Empty, config.QueueName).Result;

            var aircraftDueDate = JsonConvert.DeserializeObject<AircraftNextDue>(response);
            aircraftDueDate.Tasks?.ForEach(t =>
            {
                Console.WriteLine($"AircraftId: {aircraftDueDate.AircraftId}\tItemId: {t.ItemNumber}\tDescription: {t.Description}\tNextDue: {t.NextDue}");
            });
            
            //Console.WriteLine(" [.] Got '{0}'", response);
            rpcClient.Close();
        }

        public static void SendRmqMessage(List<TaskItem> items)
        {
            const string host = "portal1394-7.euphoric-rabbitmq-75.752837871.composedb.com";
            const int port = 16456;
            const string username = "fdox";
            const string password = "aviation1234";
            const string virtualHost = "euphoric-rabbitmq-75";
            const string exchange = "maintenance.exc.fdox";

            // https://help.compose.com/docs/rabbitmq-on-compose
            var sslOPtion = new SslOption
            {
                Enabled = true,
                ServerName = host,
                Version = SslProtocols.Tls12
            };

            var factory = new ConnectionFactory
            {
                UserName = username,
                Password = password,
                VirtualHost = virtualHost,
                Port = port,
                HostName = host,
                Ssl = sslOPtion
            };

            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                var msg = JsonConvert.SerializeObject(items);
                var body = Encoding.UTF8.GetBytes(msg);

                channel.ExchangeDeclare(exchange: exchange, type: "fanout", durable: true);

                channel.BasicPublish(exchange: exchange,
                    routingKey: "",
                    basicProperties: null,
                    body: body);
            }
        }

        private static Aircraft GetTestAircraft(int id)
        {
            if (id == 1)
                return new Aircraft()
                {
                    Id = 1,
                    CurrentHours = 550,
                    DailyHours = 0.7m
                };

            return new Aircraft()
            {
                Id = 2,
                CurrentHours = 200,
                DailyHours = 1.1m
            };
        }

        private static List<TaskItem> GenerateTestTasks()
        {
            return new List<TaskItem>()
            {
                new TaskItem()
                {
                    ItemNumber = 1,
                    Description = "Item 1",
                    LogDate = new DateTime(2018, 4, 7)
                },
                new TaskItem()
                {
                    ItemNumber = 2,
                    Description = "Item 2",
                    LogDate = new DateTime(2018, 4, 7),
                    LogHours = 100,
                    IntervalMonths = 12,
                    IntervalHours = 500
                },
                new TaskItem()
                {
                    ItemNumber = 3,
                    Description = "Item 3",
                    LogDate = new DateTime(2018, 6, 1),
                    LogHours = 150,
                    IntervalHours = 400
                },
                new TaskItem()
                {
                    ItemNumber = 4,
                    Description = "Item 4",
                    LogDate = new DateTime(2018, 6, 1),
                    LogHours = 150,
                    IntervalMonths = 6
                }
            };
        }
    }
}