﻿using System;

namespace fDox.Common.Dtos
{
    public class TaskNextDueItem : TaskItem
    {
        public DateTime? NextDue { get; set; }
    }
}