﻿using System.Collections.Generic;

namespace fDox.Common.Dtos
{
    public class AircraftNextDue
    {
        public int AircraftId { get; set; }
        public List<TaskNextDueItem> Tasks { get; set; } = new List<TaskNextDueItem>();
    }
}