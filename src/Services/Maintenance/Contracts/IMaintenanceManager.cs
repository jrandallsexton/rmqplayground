﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using fDox.Common.Dtos;
using fDox.Common.Shared;

namespace fDox.Maintenance.Contracts
{
    public interface IMaintenanceManager
    {
        Task<AircraftNextDue> CalculateNextDueDates(UpdateAircraftDueDateCommand updateAircraftDueDateCommand);
    }
}
