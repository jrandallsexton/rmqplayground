﻿
using System;
using System.Collections.Generic;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;

using fDox.Aircraft.Contracts;
using fDox.Common.Dtos;
using fDox.Common.Messaging;
using fDox.Common.Shared;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RabbitMQ.Client;

namespace fDox.Aircraft.Application
{
    public class AircraftManager : IAircraftManager
    {
        private readonly IAircraftRepository _aircraftRepository;
        private readonly IConfiguration _configuration;
        private readonly MessageServerConfig _messageServerConfig;
        private readonly RmqLogger _logger;

        public AircraftManager(IAircraftRepository aircraftRepository, IConfiguration configuration)
        {
            _aircraftRepository = aircraftRepository;
            _configuration = configuration;
            _messageServerConfig = BuildMessageServerConfig(configuration);
            _logger = new RmqLogger(_messageServerConfig);
        }

        public async Task<AircraftNextDue> GenerateNextDue(int aircraftId, List<TaskItem> maintenanceItems)
        {
            var aircraft = await _aircraftRepository.GetByIdAsync(aircraftId);
            _logger.Log(LogLevel.Information, 0, null, new Exception("blah blah blah"));
            // generate the command
            var generateCommand = new UpdateAircraftDueDateCommand()
            {
                Aircraft = aircraft,
                Tasks = maintenanceItems
            };

            // publish the message
            return await SendRmqMessageViaRpc(_messageServerConfig, generateCommand);
        }

        private static MessageServerConfig BuildMessageServerConfig(IConfiguration configuration)
        {
            return new MessageServerConfig
            {
                Host = configuration["MessageServerConfig:Host"],
                Port = int.Parse(configuration["MessageServerConfig:Port"]),
                Username = configuration["MessageServerConfig:Username"],
                Password = configuration["MessageServerConfig:Password"],
                VirtualHost = configuration["MessageServerConfig:VirtualHost"],
                ExchangeName = string.Empty,
                QueueName = configuration["MessageServerConfig:QueueName"]
            };
        }

        private async Task<AircraftNextDue> SendRmqMessageViaRpc(MessageServerConfig config, UpdateAircraftDueDateCommand command)
        {

            var rpcClient = new RpcClient(config);
            Console.WriteLine("Sending");
            var msgValue = JsonConvert.SerializeObject(command);
            var response = await rpcClient.CallAsync(msgValue, string.Empty, config.QueueName);
            rpcClient.Close();
            return JsonConvert.DeserializeObject<AircraftNextDue>(response);
        }

        public static void SendRmqMessage(List<TaskItem> items)
        {
            const string host = "portal1394-7.euphoric-rabbitmq-75.752837871.composedb.com";
            const int port = 16456;
            const string username = "fdox";
            const string password = "aviation1234";
            const string virtualHost = "euphoric-rabbitmq-75";
            const string exchange = "maintenance.exc.fdox";
            const string replyQueueName = "aircraft.que.fdox";

            // https://help.compose.com/docs/rabbitmq-on-compose
            var sslOPtion = new SslOption
            {
                Enabled = true,
                ServerName = host,
                Version = SslProtocols.Tls12
            };

            var factory = new ConnectionFactory
            {
                UserName = username,
                Password = password,
                VirtualHost = virtualHost,
                Port = port,
                HostName = host,
                Ssl = sslOPtion
            };

            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                var msg = JsonConvert.SerializeObject(items);
                var body = Encoding.UTF8.GetBytes(msg);

                channel.ExchangeDeclare(exchange: exchange, type: "fanout", durable: true);

                var props = channel.CreateBasicProperties();
                props.ReplyTo = replyQueueName;

                channel.BasicPublish(exchange: exchange,
                    routingKey: "rpc_queue",
                    basicProperties: props,
                    body: body);
            }
        }
    }
}