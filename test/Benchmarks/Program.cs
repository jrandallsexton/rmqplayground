﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;
using fDox.Common.Dtos;
using fDox.Common.Shared;

namespace Benchmarks
{
    class Program
    {
        static void Main(string[] args)
        {
            var summary = BenchmarkRunner.Run<CalculateNextDueDatesBenchmarks>();
            //Console.WriteLine("Press any key to exit");
            //Console.ReadLine();
        }
    }

    public class CalculateNextDueDatesBenchmarks
    {
        private readonly UpdateAircraftDueDateCommand _command;
        private readonly Aircraft _aircraft;
        private readonly DateTime? _logDate;
        private readonly decimal? _logHours;
        private readonly int? _intervalMonths;
        private readonly decimal? _intervalHours;

        public CalculateNextDueDatesBenchmarks()
        {
            _aircraft = new Aircraft()
            {
                Id = 1,
                CurrentHours = 550,
                DailyHours = 0.7m
            };
            _logDate = new DateTime(2018, 4, 7);
            _logHours = 100;
            _intervalMonths = 12;
            _intervalHours = 500;

            var tasks = GenerateTestTasks(25);

            _command = new UpdateAircraftDueDateCommand()
            {
                Aircraft = _aircraft,
                Tasks = tasks
            };

        }

        [Benchmark(Baseline = true)]
        public Task<AircraftNextDue> CalculateNextDueDatesExec() => CalculateNextDueDates(_command);
        public async Task<AircraftNextDue> CalculateNextDueDates(UpdateAircraftDueDateCommand updateAircraftDueDateCommand)
        {
            var aircraftNextDue = new AircraftNextDue { AircraftId = updateAircraftDueDateCommand.Aircraft.Id };

            var aircraft = updateAircraftDueDateCommand.Aircraft;
            updateAircraftDueDateCommand.Tasks.ForEach(t =>
            {
                var nextDue = NextDueFormulaRules(aircraft, t.LogDate, t.LogHours, t.IntervalMonths, t.IntervalHours);
                aircraftNextDue.Tasks.Add(new TaskNextDueItem()
                {
                    Description = t.Description,
                    IntervalHours = t.IntervalHours,
                    IntervalMonths = t.IntervalMonths,
                    ItemNumber = t.ItemNumber,
                    LogDate = t.LogDate,
                    LogHours = t.LogHours,
                    NextDue = nextDue
                });
            });
            aircraftNextDue.Tasks = aircraftNextDue.Tasks.OrderBy(o => o.NextDue).ThenBy(d => d.Description)
                .ToList();

            return aircraftNextDue;
        }

        [Benchmark]
        public Task<AircraftNextDue> CalculateNextDueDates_StandardFor_Exec() => CalculateNextDueDates_StandardFor(_command);
        public async Task<AircraftNextDue> CalculateNextDueDates_StandardFor(UpdateAircraftDueDateCommand updateAircraftDueDateCommand)
        {
            var aircraftNextDue = new AircraftNextDue { AircraftId = updateAircraftDueDateCommand.Aircraft.Id };

            var aircraft = updateAircraftDueDateCommand.Aircraft;
            foreach (var t in updateAircraftDueDateCommand.Tasks)
            {
                var nextDue = NextDueFormulaRules(aircraft, t.LogDate, t.LogHours, t.IntervalMonths, t.IntervalHours);
                aircraftNextDue.Tasks.Add(new TaskNextDueItem()
                {
                    Description = t.Description,
                    IntervalHours = t.IntervalHours,
                    IntervalMonths = t.IntervalMonths,
                    ItemNumber = t.ItemNumber,
                    LogDate = t.LogDate,
                    LogHours = t.LogHours,
                    NextDue = nextDue
                });
            }

            aircraftNextDue.Tasks = aircraftNextDue.Tasks.OrderBy(o => o.NextDue).ThenBy(d => d.Description)
                .ToList();

            return aircraftNextDue;
        }

        [Benchmark]
        public Task<AircraftNextDue> CalculateNextDueDates_ForCounter_Exec() => CalculateNextDueDates_ForCounter(_command);
        public async Task<AircraftNextDue> CalculateNextDueDates_ForCounter(UpdateAircraftDueDateCommand updateAircraftDueDateCommand)
        {
            var aircraftNextDue = new AircraftNextDue { AircraftId = updateAircraftDueDateCommand.Aircraft.Id };

            var aircraft = updateAircraftDueDateCommand.Aircraft;
            var taskCount = updateAircraftDueDateCommand.Tasks.Count;
            for (var i=0; i < taskCount; i++)
            {
                var t = updateAircraftDueDateCommand.Tasks[i];
                var nextDue = NextDueFormulaRules(aircraft, t.LogDate, t.LogHours, t.IntervalMonths, t.IntervalHours);
                aircraftNextDue.Tasks.Add(new TaskNextDueItem()
                {
                    Description = t.Description,
                    IntervalHours = t.IntervalHours,
                    IntervalMonths = t.IntervalMonths,
                    ItemNumber = t.ItemNumber,
                    LogDate = t.LogDate,
                    LogHours = t.LogHours,
                    NextDue = nextDue
                });
            }

            aircraftNextDue.Tasks = aircraftNextDue.Tasks.OrderBy(o => o.NextDue).ThenBy(d => d.Description)
                .ToList();

            return aircraftNextDue;
        }

        //[Benchmark]
        public Task<AircraftNextDue> CalculateNextDueDatesParellelExec() => CalculateNextDueDatesParellel(_command);
        public async Task<AircraftNextDue> CalculateNextDueDatesParellel(UpdateAircraftDueDateCommand updateAircraftDueDateCommand)
        {
            var aircraftNextDue = new AircraftNextDue { AircraftId = updateAircraftDueDateCommand.Aircraft.Id };

            var aircraft = updateAircraftDueDateCommand.Aircraft;

            Parallel.ForEach(updateAircraftDueDateCommand.Tasks, (t) =>
            {
                var nextDue = NextDueFormulaRules(aircraft, t.LogDate, t.LogHours, t.IntervalMonths, t.IntervalHours);
                aircraftNextDue.Tasks.Add(new TaskNextDueItem()
                {
                    Description = t.Description,
                    IntervalHours = t.IntervalHours,
                    IntervalMonths = t.IntervalMonths,
                    ItemNumber = t.ItemNumber,
                    LogDate = t.LogDate,
                    LogHours = t.LogHours,
                    NextDue = nextDue
                });
            });

            aircraftNextDue.Tasks = aircraftNextDue.Tasks.OrderBy(o => o.NextDue).ThenBy(d => d.Description)
                .ToList();

            return aircraftNextDue;
        }

        //[Benchmark]
        public DateTime? NextDueFormulaRulesExec() =>
            NextDueFormulaRules(_aircraft, _logDate, _logHours, _intervalMonths, _intervalHours);
        public DateTime? NextDueFormulaRules(Aircraft aircraft, DateTime? logDate, decimal? logHours,
            int? intervalMonths, decimal? intervalHours)
        {

            // For demo purposes, Today = 6/19/2018
            var today = new DateTime(2018, 6, 19);

            //IntervalMonthsNextDueDate = LogDate + IntervalMonths 
            //  (IntervalMonthsNextDueDate will be null if either LogDate or IntervalMonths are null)
            var intervalMonthsNextDueDate = (logDate == null || !intervalMonths.HasValue)
                ? (DateTime?)null
                : logDate.Value.AddMonths(intervalMonths.Value);

            //DaysRemainingByHoursInterval = ((LogHours + IntervalHours) - MockedAircraft.CurrentHours) / DailyHours
            //  (DaysRemainingByHoursInterval could be null)
            var daysRemainingByHoursInterval = (logHours.HasValue && intervalHours.HasValue)
                ? ((logHours.Value + intervalHours.Value) - aircraft.CurrentHours) / aircraft.DailyHours
                : (decimal?)null;

            //IntervalHoursNextDueDate =  DaysRemainingByHours + Today.
            //  (IntervalHoursNextDueDate could be null)
            var intervalHoursNextDueDate = daysRemainingByHoursInterval.HasValue
                ? today.AddDays((int)daysRemainingByHoursInterval)
                : (DateTime?)null;

            //var nextDueDate = MIN(IntervalMonthsNextDueDate, IntervalHoursNextDueDate) OR Null
            if (!intervalHoursNextDueDate.HasValue && !intervalMonthsNextDueDate.HasValue)
                return null;

            intervalHoursNextDueDate = intervalHoursNextDueDate ?? DateTime.MaxValue;
            intervalMonthsNextDueDate = intervalMonthsNextDueDate ?? DateTime.MaxValue;

            return intervalMonthsNextDueDate < intervalHoursNextDueDate
                ? intervalMonthsNextDueDate
                : intervalHoursNextDueDate;

        }

        //[Benchmark]
        public DateTime? NextDueFormulaRulesAltExec() =>
            NextDueFormulaRulesAlt(_aircraft, _logDate, _logHours, _intervalMonths, _intervalHours);
        public DateTime? NextDueFormulaRulesAlt(Aircraft aircraft, DateTime? logDate, decimal? logHours,
            int? intervalMonths, decimal? intervalHours)
        {
            if (intervalHours == null && intervalMonths == null)
                return (DateTime?) null;

            // For demo purposes, Today = 6/19/2018
            var today = new DateTime(2018, 6, 19);

            //IntervalMonthsNextDueDate = LogDate + IntervalMonths 
            //  (IntervalMonthsNextDueDate will be null if either LogDate or IntervalMonths are null)
            var intervalMonthsNextDueDate = (logDate == null || !intervalMonths.HasValue)
                ? (DateTime?)null
                : logDate.Value.AddMonths(intervalMonths.Value);

            //DaysRemainingByHoursInterval = ((LogHours + IntervalHours) - MockedAircraft.CurrentHours) / DailyHours
            //  (DaysRemainingByHoursInterval could be null)
            var daysRemainingByHoursInterval = (logHours.HasValue && intervalHours.HasValue)
                ? ((logHours.Value + intervalHours.Value) - aircraft.CurrentHours) / aircraft.DailyHours
                : (decimal?)null;

            //IntervalHoursNextDueDate =  DaysRemainingByHours + Today.
            //  (IntervalHoursNextDueDate could be null)
            var intervalHoursNextDueDate = daysRemainingByHoursInterval.HasValue
                ? today.AddDays((int)daysRemainingByHoursInterval)
                : (DateTime?)null;

            //var nextDueDate = MIN(IntervalMonthsNextDueDate, IntervalHoursNextDueDate) OR Null
            if (!intervalHoursNextDueDate.HasValue && !intervalMonthsNextDueDate.HasValue)
                return null;

            intervalHoursNextDueDate = intervalHoursNextDueDate ?? DateTime.MaxValue;
            intervalMonthsNextDueDate = intervalMonthsNextDueDate ?? DateTime.MaxValue;

            return intervalMonthsNextDueDate < intervalHoursNextDueDate
                ? intervalMonthsNextDueDate
                : intervalHoursNextDueDate;

        }

        public static List<TaskItem> GenerateTestTasks(int numberToGenerate)
        {
            var tasks = new List<TaskItem>();

            for (var i = 0; i < numberToGenerate; i++)
            {
                tasks.AddRange(GenerateTestTasks());
            }
            return tasks;
        }

        public static List<TaskItem> GenerateTestTasks()
        {
            return new List<TaskItem>()
            {
                new TaskItem()
                {
                    ItemNumber = 1,
                    Description = "Item 1",
                    LogDate = new DateTime(2018, 4, 7)
                },
                new TaskItem()
                {
                    ItemNumber = 2,
                    Description = "Item 2",
                    LogDate = new DateTime(2018, 4, 7),
                    LogHours = 100,
                    IntervalMonths = 12,
                    IntervalHours = 500
                },
                new TaskItem()
                {
                    ItemNumber = 3,
                    Description = "Item 3",
                    LogDate = new DateTime(2018, 6, 1),
                    LogHours = 150,
                    IntervalHours = 400
                },
                new TaskItem()
                {
                    ItemNumber = 4,
                    Description = "Item 4",
                    LogDate = new DateTime(2018, 6, 1),
                    LogHours = 150,
                    IntervalMonths = 6
                }
            };
        }
    }
}
