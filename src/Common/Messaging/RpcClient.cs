﻿using System;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Nito.AsyncEx;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace fDox.Common.Messaging
{
    public interface IRpcClient
    {
        Task<string> CallAsync(string msg, string exchange, string routingKey);
        void Close();
    }

    public class RpcClient : IRpcClient
    {
        private readonly IConnection _connection;
        private readonly IModel _channel;
        private readonly string _replyQueueName;
        private readonly EventingBasicConsumer _consumer;
        private readonly AsyncCollection<string> _respQueue = new AsyncCollection<string>();
        private readonly IBasicProperties _props;

        private readonly MessageServerConfig _config;

        public RpcClient(MessageServerConfig config)
        {
            // https://help.compose.com/docs/rabbitmq-on-compose
            var sslOPtion = new SslOption
            {
                Enabled = true,
                ServerName = config.Host,
                Version = SslProtocols.Tls12
            };

            var factory = new ConnectionFactory
            {
                UserName = config.Username,
                Password = config.Password,
                VirtualHost = config.VirtualHost,
                Port = config.Port,
                HostName = config.Host,
                Ssl = sslOPtion
            };

            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _replyQueueName = _channel.QueueDeclare().QueueName;
            _consumer = new EventingBasicConsumer(_channel);

            _props = _channel.CreateBasicProperties();
            var correlationId = Guid.NewGuid().ToString();
            _props.CorrelationId = correlationId;
            _props.ReplyTo = _replyQueueName;

            _consumer.Received += (model, ea) =>
            {
                var body = ea.Body;
                var response = Encoding.UTF8.GetString(body);
                if (ea.BasicProperties.CorrelationId == correlationId)
                {
                    _respQueue.Add(response);
                }
            };
        }
        
        public async Task<string> CallAsync(string msg, string exchange, string routingKey)
        {
            //var msg = JsonConvert.SerializeObject<T>(item);
            var body = Encoding.UTF8.GetBytes(msg);

            _channel.BasicPublish(
                exchange: "",
                routingKey: "aircraft.que.fdox",
                basicProperties: _props,
                body: body);

            _channel.BasicConsume(
                consumer: _consumer,
                queue: _replyQueueName,
                autoAck: true);

            return await _respQueue.TakeAsync();
        }

        public void Close()
        {
            _connection.Close();
        }
    }
}