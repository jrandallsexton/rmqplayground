﻿using System.Threading.Tasks;
using fDox.Common.Dtos;
using fDox.Common.Shared;

namespace fDox.Common.Contracts
{
    public interface IMaintenanceManager
    {
        Task<AircraftNextDue> CalculateNextDueDates(UpdateAircraftDueDateCommand updateAircraftDueDateCommand);
    }
}