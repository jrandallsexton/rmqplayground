﻿
using System.Collections.Generic;
using System.Threading.Tasks;

using fDox.Common.Dtos;

namespace fDox.Aircraft.Contracts
{
    public interface IAircraftManager
    {
        Task<AircraftNextDue> GenerateNextDue(int aircraftId, List<TaskItem> maintenanceItems);
    }
}