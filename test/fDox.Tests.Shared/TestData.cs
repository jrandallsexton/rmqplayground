﻿
using System;
using System.Collections.Generic;

using fDox.Common.Dtos;

namespace fDox.Tests.Shared
{
    public class TestData
    {
        public static Aircraft GetTestAircraft(int id)
        {
            if (id == 1)
                return new Aircraft()
                {
                    Id = 1,
                    CurrentHours = 550,
                    DailyHours = 0.7m
                };

            return new Aircraft()
            {
                Id = 2,
                CurrentHours = 200,
                DailyHours = 1.1m
            };
        }

        public static List<TaskItem> GenerateTestTasks()
        {
            return new List<TaskItem>()
            {
                new TaskItem()
                {
                    ItemNumber = 1,
                    Description = "Item 1",
                    LogDate = new DateTime(2018, 4, 7)
                },
                new TaskItem()
                {
                    ItemNumber = 2,
                    Description = "Item 2",
                    LogDate = new DateTime(2018, 4, 7),
                    LogHours = 100,
                    IntervalMonths = 12,
                    IntervalHours = 500
                },
                new TaskItem()
                {
                    ItemNumber = 3,
                    Description = "Item 3",
                    LogDate = new DateTime(2018, 6, 1),
                    LogHours = 150,
                    IntervalHours = 400
                },
                new TaskItem()
                {
                    ItemNumber = 4,
                    Description = "Item 4",
                    LogDate = new DateTime(2018, 6, 1),
                    LogHours = 150,
                    IntervalMonths = 6
                }
            };
        }
    }
}