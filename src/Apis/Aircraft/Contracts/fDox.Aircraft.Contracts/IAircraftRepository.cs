﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using fDox.Common.Dtos;

namespace fDox.Aircraft.Contracts
{
    public interface IAircraftRepository
    {
        Task<Common.Dtos.Aircraft> GetByIdAsync(int id);
    }
}