﻿using System;
using System.Collections.Generic;
using System.Security.Authentication;
using System.Text;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RabbitMQ.Client;

namespace fDox.Common.Messaging
{
    public class RmqLogger : ILogger
    {

        private readonly MessageServerConfig _messageServerConfig;

        public RmqLogger(MessageServerConfig config)
        {
            _messageServerConfig = config;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            var msg = $"Level: {logLevel}\tException:{exception.Message}";
            SendRmqMessage(_messageServerConfig, msg);
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            throw new NotImplementedException();
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            throw new NotImplementedException();
        }

        public static void SendRmqMessage(MessageServerConfig config, string msg)
        {
            const string host = "portal1394-7.euphoric-rabbitmq-75.752837871.composedb.com";
            const int port = 16456;
            const string username = "fdox";
            const string password = "aviation1234";
            const string virtualHost = "euphoric-rabbitmq-75";
            const string exchange = "logs.exc.fdox";

            // https://help.compose.com/docs/rabbitmq-on-compose
            var sslOPtion = new SslOption
            {
                Enabled = true,
                ServerName = host,
                Version = SslProtocols.Tls12
            };

            var factory = new ConnectionFactory
            {
                UserName = username,
                Password = password,
                VirtualHost = virtualHost,
                Port = port,
                HostName = host,
                Ssl = sslOPtion
            };

            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                //var msg = JsonConvert.SerializeObject(items);
                var body = Encoding.UTF8.GetBytes(msg);

                channel.ExchangeDeclare(exchange: exchange, type: "topic", durable: true);

                channel.BasicPublish(exchange: exchange,
                    routingKey: "",
                    basicProperties: null,
                    body: body);
            }
        }
    }
}