﻿
using System;
using System.Drawing;
using System.Security.Authentication;
using System.Text;
using fDox.Common.Contracts;
using fDox.Common.Dtos;
using fDox.Common.Shared;
using fDox.Maintenance.Application;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Console = Colorful.Console;

namespace fDox.Maintenance.Service
{
    class Program
    {

        private static readonly IMaintenanceManager _maintenanceManager = new MaintenanceManager();
        private static readonly bool _logVerbose = true;

        static void Main(string[] args)
        {
            //UseStandardConsumer();
            UseRpcServer();
        }

        private static void UseRpcServer()
        {
            const string host = "portal1394-7.euphoric-rabbitmq-75.752837871.composedb.com";
            const int port = 16456;
            const string username = "fdox";
            const string password = "aviation1234";
            const string virtualHost = "euphoric-rabbitmq-75";
            //const string exchange = "aircraft.exc.fdox";
            const string queueName = "aircraft.que.fdox";

            // https://help.compose.com/docs/rabbitmq-on-compose
            var sslOPtion = new SslOption
            {
                Enabled = true,
                ServerName = host,
                Version = SslProtocols.Tls12
            };

            var msgCount = 1;

            var factory = new ConnectionFactory
            {
                UserName = username,
                Password = password,
                VirtualHost = virtualHost,
                Port = port,
                HostName = host,
                Ssl = sslOPtion
            };

            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: queueName, durable: true, exclusive: false, autoDelete: false, arguments: null);
                channel.BasicQos(0, 1, false);
                var consumer = new EventingBasicConsumer(channel);

                consumer.Received += async (model, ea) =>
                {
                    AircraftNextDue response = null;

                    var body = ea.Body;
                    var props = ea.BasicProperties;
                    var replyProps = channel.CreateBasicProperties();
                    replyProps.CorrelationId = props.CorrelationId;

                    try
                    {
                        var eventName = ea.RoutingKey;
                        var msgValue = System.Text.Encoding.Default.GetString(ea.Body);

                        if (_logVerbose)
                            Console.WriteLine($"{msgCount} => Received: {msgValue}", Color.Green);

                        // deserialize to UpdateAircraftDueDateCommand
                        var updateCommand = JsonConvert.DeserializeObject<UpdateAircraftDueDateCommand>(msgValue);

                        msgCount++;
                        response = await _maintenanceManager.CalculateNextDueDates(updateCommand);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(" [.] " + e.Message);
                    }
                    finally
                    {

                        var msg = JsonConvert.SerializeObject(response);

                        if (_logVerbose)
                            Console.WriteLine($"Sending response {msg} back to {props.ReplyTo}");

                        var responseBody = Encoding.UTF8.GetBytes(msg);
                        channel.BasicPublish(exchange: "", routingKey: props.ReplyTo, basicProperties: replyProps, body: responseBody);
                        channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
                    }
                };

                channel.BasicConsume(queue: queueName, autoAck: false, consumer: consumer);
                Console.WriteLine("[x] Awaiting RPC requests");
                Console.WriteLine("RPC Processor now listening to RMQ", Color.Yellow);
                Console.WriteLine("Press [enter] to exit.", Color.Red);
                Console.ReadLine();
            }
        }

        private static void UseStandardConsumer()
        {
            const string host = "portal1394-7.euphoric-rabbitmq-75.752837871.composedb.com";
            const int port = 16456;
            const string username = "fdox";
            const string password = "aviation1234";
            const string virtualHost = "euphoric-rabbitmq-75";
            const string exchange = "maintenance.exc.fdox";
            const string queueName = "maintenance.que.fdox";

            // https://help.compose.com/docs/rabbitmq-on-compose
            var sslOPtion = new SslOption
            {
                Enabled = true,
                ServerName = host,
                Version = SslProtocols.Tls12
            };

            var msgCount = 1;

            var factory = new ConnectionFactory
            {
                UserName = username,
                Password = password,
                VirtualHost = virtualHost,
                Port = port,
                HostName = host,
                Ssl = sslOPtion
            };

            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    var consumer = new EventingBasicConsumer(channel);
                    consumer.Received += (ch, ea) =>
                    {
                        var eventName = ea.RoutingKey;
                        var msgValue = System.Text.Encoding.Default.GetString(ea.Body);
                        if (_logVerbose)
                            Console.WriteLine($"{msgCount} => Received: {msgValue}", Color.Green);
                        msgCount++;
                        channel.BasicAck(ea.DeliveryTag, false);
                    };
                    channel.BasicConsume(queueName, false, consumer);
                    Console.WriteLine("Processor now listening to RMQ", Color.Yellow);
                    Console.WriteLine("Press [enter] to exit.", Color.Red);
                    Console.ReadLine();
                }

            }
        }
    }
}