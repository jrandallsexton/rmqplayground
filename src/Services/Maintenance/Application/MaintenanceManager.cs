﻿using System;
using System.Linq;
using System.Threading.Tasks;
using fDox.Common.Contracts;
using fDox.Common.Dtos;
using fDox.Common.Shared;

namespace fDox.Maintenance.Application
{
    public class MaintenanceManager : IMaintenanceManager
    {
        public async Task<AircraftNextDue> CalculateNextDueDates(UpdateAircraftDueDateCommand updateAircraftDueDateCommand)
        {
            var aircraftNextDue = new AircraftNextDue { AircraftId = updateAircraftDueDateCommand.Aircraft.Id };

            var aircraft = updateAircraftDueDateCommand.Aircraft;
            updateAircraftDueDateCommand.Tasks.ForEach(t =>
            {
                var nextDue = NextDueFormulaRules(aircraft, t.LogDate, t.LogHours, t.IntervalMonths, t.IntervalHours);
                aircraftNextDue.Tasks.Add(new TaskNextDueItem()
                {
                    Description = t.Description,
                    IntervalHours = t.IntervalHours,
                    IntervalMonths = t.IntervalMonths,
                    ItemNumber = t.ItemNumber,
                    LogDate = t.LogDate,
                    LogHours = t.LogHours,
                    NextDue = nextDue
                });
            });
            aircraftNextDue.Tasks = aircraftNextDue.Tasks.OrderByDescending(o => o.NextDue.HasValue)
                .ThenBy(o => o.NextDue)
                .ThenBy(d => d.Description)
                .ToList();

            return aircraftNextDue;
        }

        private DateTime? NextDueFormulaRules(Aircraft aircraft, DateTime? logDate, decimal? logHours, int? intervalMonths, decimal? intervalHours)
        {

            // For demo purposes, Today = 6/19/2018
            var today = new DateTime(2018, 6, 19);

            //IntervalMonthsNextDueDate = LogDate + IntervalMonths 
            //  (IntervalMonthsNextDueDate will be null if either LogDate or IntervalMonths are null)
            var intervalMonthsNextDueDate = (logDate == null || !intervalMonths.HasValue)
                ? (DateTime?)null
                : logDate.Value.AddMonths(intervalMonths.Value);

            //DaysRemainingByHoursInterval = ((LogHours + IntervalHours) - MockedAircraft.CurrentHours) / DailyHours
            //  (DaysRemainingByHoursInterval could be null)
            var daysRemainingByHoursInterval = (logHours.HasValue && intervalHours.HasValue)
                ? ((logHours.Value + intervalHours.Value) - aircraft.CurrentHours) / aircraft.DailyHours
                : (decimal?)null;

            //IntervalHoursNextDueDate =  DaysRemainingByHours + Today.
            //  (IntervalHoursNextDueDate could be null)
            var intervalHoursNextDueDate = daysRemainingByHoursInterval.HasValue
                ? today.AddDays((int)daysRemainingByHoursInterval)
                : (DateTime?)null;

            //var nextDueDate = MIN(IntervalMonthsNextDueDate, IntervalHoursNextDueDate) OR Null
            if (!intervalHoursNextDueDate.HasValue && !intervalMonthsNextDueDate.HasValue)
                return null;

            intervalHoursNextDueDate = intervalHoursNextDueDate ?? DateTime.MaxValue;
            intervalMonthsNextDueDate = intervalMonthsNextDueDate ?? DateTime.MaxValue;

            return intervalMonthsNextDueDate < intervalHoursNextDueDate
                ? intervalMonthsNextDueDate
                : intervalHoursNextDueDate;

        }
    }
}