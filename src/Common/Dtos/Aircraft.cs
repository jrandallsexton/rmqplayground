﻿
namespace fDox.Common.Dtos
{
    public class Aircraft
    {
        public int Id { get; set; }
        public decimal DailyHours { get; set; }
        public decimal CurrentHours { get; set; }
    }
}