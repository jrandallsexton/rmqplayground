﻿
using System.Threading.Tasks;
using fDox.Aircraft.Contracts;

namespace fDox.Aircraft.Data
{
    public class AircraftRepository : IAircraftRepository
    {
        public async Task<Common.Dtos.Aircraft> GetByIdAsync(int id)
        {
            //System.Threading.Thread.Sleep(500);

            if (id == 1)
            {
                return new Common.Dtos.Aircraft()
                {
                    Id = id,
                    DailyHours = 0.7m,
                    CurrentHours = 550
                };
            }
            return new Common.Dtos.Aircraft()
            {
                Id = id,
                DailyHours = 1.1m,
                CurrentHours = 200
            };
        }
    }
}