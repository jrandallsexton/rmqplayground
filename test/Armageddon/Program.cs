﻿using System;
using System.Collections.Generic;
using fDox.Common.Dtos;
using fDox.Common.Messaging;
using fDox.Common.Shared;
using Newtonsoft.Json;

namespace Armageddon
{
    class Program
    {
        static void Main(string[] args)
        {
            var itemsToSend = 100;
            for (var i = 0; i < itemsToSend; i++)
            {
                for (var j = 1; j < 3; j++)
                {
                    var aircraft = GetTestAircraft(j);

                    var command = new UpdateAircraftDueDateCommand()
                    {
                        Aircraft = aircraft,
                        Tasks = GenerateTestTasks()
                    };
                    Console.WriteLine("Item generated");
                    SendRmqMessageViaRpc(command);
                }
            }

            Console.WriteLine($"{itemsToSend} items sent via RPC. Any key to exit");
            Console.ReadLine();
        }

        public static void SendRmqMessageViaRpc(UpdateAircraftDueDateCommand command)
        {
            var config = new MessageServerConfig
            {
                Host = "portal1394-7.euphoric-rabbitmq-75.752837871.composedb.com",
                Port = 16456,
                Username = "fdox",
                Password = "aviation1234",
                VirtualHost = "euphoric-rabbitmq-75",
                ExchangeName = string.Empty,
                QueueName = "aircraft.que.fdox"
            };

            var rpcClient = new RpcClient(config);
            Console.WriteLine("Sending");

            var msg = JsonConvert.SerializeObject(command);
            var response = rpcClient.CallAsync(msg, string.Empty, config.QueueName).Result;

            var aircraftDueDate = JsonConvert.DeserializeObject<AircraftNextDue>(response);
            aircraftDueDate.Tasks?.ForEach(t =>
            {
                Console.WriteLine($"AircraftId: {aircraftDueDate.AircraftId}\tItemId: {t.ItemNumber}\tDescription: {t.Description}\tNextDue: {t.NextDue}");
            });

            //Console.WriteLine(" [.] Got '{0}'", response);
            rpcClient.Close();
        }

        private static Aircraft GetTestAircraft(int id)
        {
            if (id == 1)
                return new Aircraft()
                {
                    Id = 1,
                    CurrentHours = 550,
                    DailyHours = 0.7m
                };

            return new Aircraft()
            {
                Id = 2,
                CurrentHours = 200,
                DailyHours = 1.1m
            };
        }

        private static List<TaskItem> GenerateTestTasks()
        {
            return new List<TaskItem>()
            {
                new TaskItem()
                {
                    ItemNumber = 1,
                    Description = "Item 1",
                    LogDate = new DateTime(2018, 4, 7)
                },
                new TaskItem()
                {
                    ItemNumber = 2,
                    Description = "Item 2",
                    LogDate = new DateTime(2018, 4, 7),
                    LogHours = 100,
                    IntervalMonths = 12,
                    IntervalHours = 500
                },
                new TaskItem()
                {
                    ItemNumber = 3,
                    Description = "Item 3",
                    LogDate = new DateTime(2018, 6, 1),
                    LogHours = 150,
                    IntervalHours = 400
                },
                new TaskItem()
                {
                    ItemNumber = 4,
                    Description = "Item 4",
                    LogDate = new DateTime(2018, 6, 1),
                    LogHours = 150,
                    IntervalMonths = 6
                }
            };
        }
    }
}
