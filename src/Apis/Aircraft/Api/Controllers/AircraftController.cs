﻿
using System.Collections.Generic;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;

using fDox.Aircraft.Contracts;
using fDox.Common.Dtos;
using Microsoft.Extensions.Logging;

namespace fDox.Aircraft.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AircraftController : ControllerBase
    {

        private readonly IAircraftManager _aircraftManager;
        private readonly ILogger<AircraftController> _logger;

        public AircraftController(IAircraftManager aircraftManager, ILogger<AircraftController> logger)
        {
            _aircraftManager = aircraftManager;
            _logger = logger;
        }

        [HttpPost("{id}/duelist", Name = "GetDueList")]
        [ProducesResponseType(200)]
        public async Task<IActionResult> Post(int id, [FromBody] List<TaskItem> items)
        {
            if (id < 0)
                return BadRequest("Invalid aircraft id");

            if (items == null)
                return BadRequest("No maintenance items provied");

            var response = await _aircraftManager.GenerateNextDue(id, items);
            
            return Ok(response);
        }
    }
}