﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using fDox.Common.Dtos;
using Newtonsoft.Json;
using Xunit;

using fDox.Tests.Shared;
using fDox.Common.Shared;

namespace fDox.Maintenance.Tests
{
    public class MaintenanceNextDueManagerTests
    {
        //private IAircraftRepository _aircraftRepository;
        //private IMaintenanceManager _manager;

        public MaintenanceNextDueManagerTests()
        {
            //_aircraftRepository = new AircraftRepository();
            //_manager = new MaintenanceNextDueManager();
        }

        [Fact]
        public void CommandSerializes()
        {
            var aircraft = TestData.GetTestAircraft(1);
            var command = new UpdateAircraftDueDateCommand()
            {
                Aircraft = aircraft,
                Tasks = TestData.GenerateTestTasks()
            };
            var msgValue = JsonConvert.SerializeObject(command);
            var updateCommand = JsonConvert.DeserializeObject<UpdateAircraftDueDateCommand>(msgValue);
            Assert.NotNull(updateCommand);
        }

        [Fact]
        public void NextDueDateIntervals_Calculated_Correctly_Aircraft1()
        {
            var aircraft = TestData.GetTestAircraft(1);
            var command = new UpdateAircraftDueDateCommand()
            {
                Aircraft = aircraft,
                Tasks = TestData.GenerateTestTasks()
            };

            var nextDue = CalculateNextDueDates(command);

            var expectedValues = new Dictionary<int, DateTime?>
            {
                {1, null},
                {2, new DateTime(2018, 8, 29)},
                {3, new DateTime(2018, 6, 19)},
                {4, new DateTime(2018, 12, 1)}
            };

            Assert.NotNull(nextDue);

            foreach (var kvp in expectedValues)
            {
                var result = nextDue.Tasks.First(t => t.ItemNumber == kvp.Key);
                Assert.NotNull(result);
                Assert.Equal(kvp.Value, result.NextDue);
                //Console.WriteLine($"Item {kvp.Key}\tExpected: {kvp.Value}\tActual: {result.NextDue}");
            }
        }

        [Fact]
        public void NextDueDateIntervals_Calculated_Correctly_Aircraft2()
        {
            var aircraft = TestData.GetTestAircraft(2);
            var command = new UpdateAircraftDueDateCommand()
            {
                Aircraft = aircraft,
                Tasks = TestData.GenerateTestTasks()
            };

            var nextDue = CalculateNextDueDates(command);

            var expectedValues = new Dictionary<int, DateTime?>
            {
                {1, null},
                {2, new DateTime(2019, 4, 7)},
                {3, new DateTime(2019, 5, 3)},
                {4, new DateTime(2018, 12, 1)}
            };

            Assert.NotNull(nextDue);

            foreach (var kvp in expectedValues)
            {
                var result = nextDue.Tasks.First(t => t.ItemNumber == kvp.Key);
                Assert.NotNull(result);
                Assert.Equal(kvp.Value, result.NextDue);
                //Console.WriteLine($"Item {kvp.Key}\tExpected: {kvp.Value}\tActual: {result.NextDue}");
            }
        }

        private AircraftNextDue CalculateNextDueDates(UpdateAircraftDueDateCommand updateAircraftDueDateCommand)
        {
            var aircraftNextDue = new AircraftNextDue { AircraftId = updateAircraftDueDateCommand.Aircraft.Id };

            var aircraft = updateAircraftDueDateCommand.Aircraft;
            updateAircraftDueDateCommand.Tasks.ForEach(t =>
            {
                var nextDue = NextDueFormulaRules(aircraft, t.LogDate, t.LogHours, t.IntervalMonths, t.IntervalHours);
                aircraftNextDue.Tasks.Add(new TaskNextDueItem()
                {
                    Description = t.Description,
                    IntervalHours = t.IntervalHours,
                    IntervalMonths = t.IntervalMonths,
                    ItemNumber = t.ItemNumber,
                    LogDate = t.LogDate,
                    LogHours = t.LogHours,
                    NextDue = nextDue
                });
            });
            aircraftNextDue.Tasks = aircraftNextDue.Tasks.OrderByDescending(o => o.NextDue).ThenBy(d => d.Description)
                .ToList();

            return aircraftNextDue;
        }

        private DateTime? NextDueFormulaRules(Aircraft aircraft, DateTime? logDate, decimal? logHours, int? intervalMonths, decimal? intervalHours)
        {

            // For demo purposes, Today = 6/19/2018
            var today = new DateTime(2018, 6, 19);

            //IntervalMonthsNextDueDate = LogDate + IntervalMonths 
            //  (IntervalMonthsNextDueDate will be null if either LogDate or IntervalMonths are null)
            var intervalMonthsNextDueDate = (logDate == null || !intervalMonths.HasValue)
                ? (DateTime?)null
                : logDate.Value.AddMonths(intervalMonths.Value);

            //DaysRemainingByHoursInterval = ((LogHours + IntervalHours) - MockedAircraft.CurrentHours) / DailyHours
            //  (DaysRemainingByHoursInterval could be null)
            var daysRemainingByHoursInterval = (logHours.HasValue && intervalHours.HasValue)
                ? ((logHours.Value + intervalHours.Value) - aircraft.CurrentHours) / aircraft.DailyHours
                : (decimal?)null;

            //IntervalHoursNextDueDate =  DaysRemainingByHours + Today.
            //  (IntervalHoursNextDueDate could be null)
            var intervalHoursNextDueDate = daysRemainingByHoursInterval.HasValue
                ? today.AddDays((int)daysRemainingByHoursInterval)
                : (DateTime?)null;

            //var nextDueDate = MIN(IntervalMonthsNextDueDate, IntervalHoursNextDueDate) OR Null
            if (!intervalHoursNextDueDate.HasValue && !intervalMonthsNextDueDate.HasValue)
                return null;

            intervalHoursNextDueDate = intervalHoursNextDueDate ?? DateTime.MaxValue;
            intervalMonthsNextDueDate = intervalMonthsNextDueDate ?? DateTime.MaxValue;

            return intervalMonthsNextDueDate < intervalHoursNextDueDate
                ? intervalMonthsNextDueDate
                : intervalHoursNextDueDate;

        }

    }
}
